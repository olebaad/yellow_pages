﻿using System;
using System.Collections.Generic;
using name_search;

namespace name_search
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Program.YellowPages();

            
        }
        static void YellowPages()
        {
            List<Person> nameList = new List<Person>();
            nameList.Add(new Person("Cem", "Pedersen"));
            nameList.Add(new Person("Dean", "von Schoultz"));
            nameList.Add(new Person("Jens", "Hetland"));
            nameList.Add(new Person("Marius", "Lode"));
            nameList.Add(new Person("Phillip", "Aubert"));

            List<string> searchResult = new List<string>();

            Console.Write("Please enter a query to search name list:\n");
            string input = Console.ReadLine();

            List<Person> result = SearchFunc(nameList, input);
            PrintResult(result);

        }

        static List<Person> SearchFunc(List<Person> nameList, string searchInput)
        {
            List<Person> searchResult = new List<Person>();
            int count = 0;

            foreach (Person person in nameList)
            {
                if (person.NameContainsString(searchInput))
                {
                    searchResult.Add(person);
                }
                count++;
            }

            return searchResult;

        }

        static void PrintResult(List<Person> listToPrint)
        {
            int count = 1;
            Console.WriteLine("\nSearch Result: \n");
            foreach (Person person in listToPrint)
            {
                Console.WriteLine($"{count}.\t{person.FirstName} {person.LastName}");
                count++;
            }
        }
    }
}
