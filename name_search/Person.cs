﻿using System;

namespace name_search
{
	public class Person
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Telephone { get; set; }
		public string Email { get; set; }

		public Person(string firstName, string lastName)
		{
			FirstName = firstName;
			LastName = lastName;
		}

		public Person(string firstName, string lastName, string tlf)
		{
			FirstName = firstName;
			LastName = lastName;
			Telephone = tlf;

		}

		public Person(string firstName, string lastName, string email, string tlf = "unavailable")
		{
			FirstName = firstName;
			LastName = lastName;
			Email = email;
			Telephone = tlf;
		}

		public bool NameContainsString(string searchInput)
		{
			string fullName = $"{FirstName} {LastName}";
			if (fullName.ToLower().Contains(searchInput.ToLower()))
			{
				return true;
			}

			return false;
		}
	}
}

